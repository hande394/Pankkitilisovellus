﻿using NUnit.Framework;
using Pankkitilisovellus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PankkitilisovellusTests
{
    [TestFixture]
    public class PankkitiliTest
    {
        /* Pankkitilisovellus sisältää:
         * 
         * Luokka, jonka nimi on Pankkitili.
         * Pankkitilissä on kolme toimintoa.
         * 
         * -Tallettaa rahaa
         * -Nostaa rahaa
         * -Siirtää rahaa tililtä toiselle
         * --Tarkistetaan ettei tili mene miinukselle
         * 
        */

        public Pankkitili tili1 = null;

        // OneTimeSetUp
        // TearDown
        // OneTimeTearDown


        [SetUp]
        public void TestienAlustaja()
        {

            this.tili1 = new Pankkitili(100);


        }
        [Test]
        public void LuoPankkitili()
        {
            //Pankkitili tili1 = new Pankkitili(100);
           
            // Testataan olion luokan tyyppi
            Assert.IsInstanceOf<Pankkitili>(tili1);
        }
        [Test]
        public void AsetaPankkitililleAlkusaldo()
        {
            //Pankkitili tili1 = new Pankkitili(500);
            // Testataan arvon yhtäsuuruutta
            Assert.That(100, Is.EqualTo(tili1.Saldo));

        }
        [Test]
        public void TalletaRahaaPankkitilille()
        {
            //Pankkitili tili1 = new Pankkitili(750);
            // Talletetaan rahaa tilille
            tili1.Talleta(250);
            Assert.That(350, Is.EqualTo(tili1.Saldo));
        }

        [Test]
        public void NostaPankkitililtaRahaa()
        {

            tili1.NostaRahaa(75);
            Assert.That(25, Is.EqualTo(tili1.Saldo));

        }

        [Test]
        public void NostonJalkeenPankkitiliEiVoiOllaMiinuksella()
        {

            // Testataan antaako ohjelma halutun virhetyypin
            Assert.Throws<ArgumentException>(() => tili1.NostaRahaa(175));
            // Vaikka virhe sattuu niin rahoja ei menetetä
            // Pitäisi olla loppusaldon kanssa sama
            Assert.That(100, Is.EqualTo(tili1.Saldo));

        }
        /*[TestCase(450, 200]
        public void NostaPankkitililtaRahaa2(int alkusaldo, int nostoMaara, int saldo)
        {
            Pankkitili tili1 = new Pankkitili(alkusaldo);

            tili1.NostaRahaa(nostoMaara);


            Assert.That */


        
    }
}
